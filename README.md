# Drought tolerance traits in Neotropical trees correlate with the composition of phyllosphere fungal communities

Cambon MC., Cartry D., Chancerel E., Ziegler C., Levionnois S., Coste S., Stahl C., Delzon S., Buée M., Burban B., Cazal J., Fort T., Goret J-Y., Heuret P., Léger P., Louisanna E., Ritter Y., Bonal D., Roy M., Schimann H., Vacher C.

This repository contains all the files and scripts used to generate results and figures
for the paper entitled "Drought tolerance traits in Neotropical trees correlate with the composition of phyllosphere fungal communities"

* Raw sequence data can be downloaded from https://www.ebi.ac.uk/ena/browser/view/PRJEB42566
* Scripts and output files for sequence bioinformatic processing are in sequences_processing_workflow/
* Sample metadata and drought tolerance traits data are in data/
* The script for generating analysis and figures are in draft_drought_paracou.Rmd and draft_drought_paracou.html wich contains a session_info() with packages loaded and versions
* Intermediate result files are in results/
* Analysis outputs are in figures/ tables/ and supp_mat/
